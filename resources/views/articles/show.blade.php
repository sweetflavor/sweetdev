<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Show article</title>
  </head>
  <body>
    <h1>{{ $articles->title }}</h1>
    <div class="showBody">
      <p>{{ $articles->body }}</p>
    </div>
    <span class="data">{{ $articles->publishted_on }}</span>

  </body>
</html>
