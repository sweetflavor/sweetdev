<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Articles List</title>
  </head>
  <body>
    <h1>Articles</h1>

    @foreach ($articles as $article)
      <h2>
        <a href="{{ url('/articles', $article->id) }}">{{ $article->title }}</a>
      </h2>
      <div class="articleBody">
        {{ $article->body }}
      </div>
    @endforeach
  </body>
</html>
